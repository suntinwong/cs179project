﻿// This code is not original but referenced from the following:
// http://azerdark.wordpress.com/2010/04/23/multi-page-application-in-wpf/



using System.Windows.Controls;

namespace WpfApplication1
{
    public static class Switcher
    {
        public static iTEU_App iTEU_App;

        public static void Switch(UserControl newPage)
        {
            iTEU_App.Navigate(newPage);
        }

        public static void Switch(UserControl newPage, object state)
        {
            iTEU_App.Navigate(newPage, state);
        }
    }
}

