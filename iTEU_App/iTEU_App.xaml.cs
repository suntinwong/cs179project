﻿// This code is not original but referenced from the following:
// http://azerdark.wordpress.com/2010/04/23/multi-page-application-in-wpf/

using System;
using System.Windows;
using System.Windows.Controls;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class iTEU_App : Window
    {
        public iTEU_App()
        {
            InitializeComponent();
            Switcher.iTEU_App = this;
            Switcher.Switch(new Login());
        }

        public void Navigate(UserControl nextPage)
        {
            this.Content = nextPage;
        }

        public void Navigate(UserControl nextPage, object state)
        {
            this.Content = nextPage;
            ISwitchable s = nextPage as ISwitchable;

            if (s != null)
                s.UtilizeState(state);
            else
                throw new ArgumentException("NextPage is not ISwitchable! "
                  + nextPage.Name.ToString());
        }
    }
}

