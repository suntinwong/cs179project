﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration; //used to access settings.settings

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for EditAcc.xaml
    /// </summary>
    public partial class EditAcc : UserControl, ISwitchable
    {

        string username, password, newpassword, newpassword2;
        string actualusername, actualpassword, actuallevel;

        public EditAcc()
        {
            InitializeComponent();

            //Determine the username and account level
            string rawstring = Properties.Settings.Default.current_user.ToString(); //get the rawstring
            actualusername = rawstring.Substring(0, rawstring.IndexOf(" "));        //get the username
            rawstring = rawstring.Substring(rawstring.IndexOf(" ") + 1);
            actualpassword = rawstring.Substring(0, rawstring.IndexOf(" "));        //get password
            rawstring = rawstring.Substring(rawstring.IndexOf(" ") + 1);
            actuallevel = rawstring;         //get the security level

            Username.Text = actualusername; newpassword = " "; newpassword2 = "  ";
        }

        //When confirm button is clicked, do
        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {

            //check if username already exists
            bool nameexists = false;
             foreach (SettingsProperty  currentProperty in Properties.Settings.Default.Properties){
                string resourceKey = currentProperty.Name.ToString();
                string resourceVal = Properties.Settings.Default[currentProperty.Name].ToString();
                if (resourceKey.Length > 4 && resourceKey.Substring(0, 4) == "user"){
                    string temp = resourceVal.Substring(0, resourceVal.IndexOf(" "));
                    if (temp == username && temp != actualusername) { nameexists = true; }
                }
             }

            //if everything is a valid input
            if (username.Length > 0 && username.Length <= 16 && password == actualpassword
                && newpassword == newpassword2 && newpassword.Length <= 20 && newpassword.Length >= 6
                && !nameexists){

                    //make the new info string
                    string newinfo = username + " " + newpassword + " " + actuallevel;

                    //update userlog
                    if(actualusername != username){
                        string logstring =GlobalFunctions.gettime() + " \"" + actualusername 
                        + "\" changed name to \"" + username + "\"" + System.Environment.NewLine;
                        Properties.Settings.Default.loguser += logstring; 
                    }

                    //find the user in settings.settings
                    foreach (SettingsProperty currentProperty in Properties.Settings.Default.Properties){
                        string resourceKey = currentProperty.Name.ToString();
                        string resourceVal = Properties.Settings.Default[currentProperty.Name].ToString();
                        if (resourceKey.Length > 4 && resourceKey.Substring(0, 4) == "user"){
                            //once found, set the new info
                            if (resourceVal.Substring(0, resourceVal.IndexOf(" ")) == actualusername){
                                Properties.Settings.Default[currentProperty.Name] = newinfo; //set settings to newinfo
                                Properties.Settings.Default.current_user = newinfo;          //set current to newinfo
                                Properties.Settings.Default.Save();                          //save everything
                                MessageBoxResult popup = MessageBox.Show("Settings Saved!", "Sucess!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                Switcher.Switch(new Options());                              //switch screens
                            }
                        }
                    }
            }

            //When user inputted invalid stuff
            else{
                string invalidtext = "Invalid!";

                //invalid input; too long username
                if (username.Length > 16) invalidtext += System.Environment.NewLine +  "Username is too long.";
                //username is too short
                if (username.Length < 1) invalidtext += System.Environment.NewLine + "Username is too short.";
                //username already exists
                if (nameexists) invalidtext += System.Environment.NewLine + "Username already exists.";
                //actual password does not match password
                if (password != actualpassword) invalidtext += System.Environment.NewLine + "Incorrect current password.";
                //new password is too short
                if (newpassword.Length < 6) invalidtext += System.Environment.NewLine + "New password is too short.";
                //new password is too long
                if (newpassword.Length > 20) invalidtext += System.Environment.NewLine + "New password is too long.";
                //if new passwordws do not match
                if (newpassword != newpassword2) invalidtext += System.Environment.NewLine + "New passwords do not match.";
               

                //make invalid popup
                MessageBoxResult popup = MessageBox.Show(invalidtext,"Invalid",MessageBoxButton.OK,MessageBoxImage.Exclamation);

                
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new Options());
        }


        //Username textbox typing action
        private void Username_TextChanged(object sender, TextChangedEventArgs e){
            username = sender.ToString().Substring(sender.ToString().IndexOf(" ") + 1);
        }

        //Username keydown action
        private void Username_KeyDown(object sender, KeyEventArgs e){
            if (e.Key == Key.Return) //if enter key is pressed, then
                ConfirmButton_Click(sender, e);
        }

        //Password box typing action
        private void passwordBox1_PasswordChanged(object sender, RoutedEventArgs e){
            password = passwordBox1.Password;
        }

        //Password box keydown action
        private void passwordBox1_KeyDown(object sender, KeyEventArgs e){
            if (e.Key == Key.Return) //if enter key is pressed, then
                ConfirmButton_Click(sender, e);
        }

        //New Password box typing action
        private void passwordBox2_PasswordChanged(object sender, RoutedEventArgs e){
            newpassword = passwordBox2.Password;
        }

        //New Password box keydown action
        private void passwordBox2_KeyDown(object sender, KeyEventArgs e){
            if (e.Key == Key.Return) //if enter key is pressed, then
                ConfirmButton_Click(sender, e);
        }

        //new Password confirm box typing action
        private void passwordBox3_PasswordChanged(object sender, RoutedEventArgs e){
            newpassword2 = passwordBox3.Password;
        }

        //new Password confirm box keydown action
        private void passwordBox3_KeyDown(object sender, KeyEventArgs e){
            if (e.Key == Key.Return) //if enter key is pressed, then
                ConfirmButton_Click(sender, e);
        }

        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion



    }
}
