﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO; //for input/output of a file
using Microsoft.VisualBasic;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Extractor : UserControl, ISwitchable
    {

        //Constants for the position of the grid, truck and buffer
        private const int GRIDLL_POSX = 265; private const int GRIDLL_POSY = 305;
        private const int TRUCK_POSX = 80; private const int TRUCK_POSY = 343;
        private const int BUFFER_POSX = 715; private const int BUFFER_POSY = 343;

        //Global varibes for the class
        private string manifest = ""; //stores the manifest in one big string
        private string TEUextract; //which TEU to currently extract
        private TEUGrid teugrid; //TEU container class
        private Button[,] buttons = new Button[Properties.Settings.Default.MaxWidth, Properties.Settings.Default.MaxDock];
        private Button buffer = new Button(); //gui button for the buffer
        private Button truck = new Button(); //gui button for teh truck
        private string manifest_filename; //stores the manifest filelocation
        private int goalx, goaly, instruction_num = 0; 
        private int instruction_state = -1;      //current instruction state
        private List<Tuple<TEUGrid, string, int, int, int, int>> instructions; //list of instructions to complete push/pull
        private Rectangle r, r2; //targeting square for moving instructions
        private bool casting_off = false; //if the current instruction is casting off or not

        ///////////////////////////////////////////////////////////////////
        ///////////////////////     CONSTRUCTOR      //////////////////////
        ///////////////////////////////////////////////////////////////////
        public Extractor(){
            InitializeComponent();

            //Properties.Settings.Default.currentmanifest = "";
            //Properties.Settings.Default.buffer = "";

            //create the target rectangles
            r = new Rectangle{ Height = 26, Width = 36, StrokeThickness =4, Stroke = Brushes.Red, Opacity = 0};
            r2 = new Rectangle { Height = 26, Width = 36, StrokeThickness = 4, Stroke = Brushes.Salmon,Opacity = 0};
            mygrid.Children.Add(r);
            mygrid.Children.Add(r2);

            //create y axis grid references
            for (int i = 0;  i < Properties.Settings.Default.MaxDock; i++) {
                TextBox t = new TextBox() {
                    Height = 20, Width = 30,
                    Margin = new Thickness(GRIDLL_POSX - 35, GRIDLL_POSY - (22.5 * i), 0, 0), Background = Brushes.Yellow,
                    Text = (i + 1).ToString(), FontSize = 12,
                    HorizontalContentAlignment = HorizontalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center,
                    IsReadOnly = true
                };
                mygrid.Children.Add(t);
            }

            //create x axis grid references
            for (int i = 0; i < Properties.Settings.Default.MaxWidth ; i++) {
                TextBox t = new TextBox() {
                    Height = 20, Width = 30,
                    Margin = new Thickness(GRIDLL_POSX + (34 * i), GRIDLL_POSY + 22.5, 0, 0), Background = Brushes.Yellow,
                    Text = GlobalFunctions.stringconvert(i + 1), FontSize = 12,
                    HorizontalContentAlignment = HorizontalAlignment.Center,HorizontalAlignment = HorizontalAlignment.Center,
                    IsReadOnly = true
               
                };
                mygrid.Children.Add(t); //add to xaml gui group
            }

            //Load automatically a manifest file if needed
            if (Properties.Settings.Default.currentmanifest != "") {

                //get fle contents, print to console
                string s = Properties.Settings.Default.currentmanifest;
                StreamReader reader = new StreamReader(s);
                while (reader.Peek() != -1) manifest += reader.ReadLine() + System.Environment.NewLine;
                teugrid = new TEUGrid(manifest); //Shove manifest into the TEUGrid contaniner
                reader.Close();
                manifest_filename = s;
                Consolebox.Text += "\"" + s.Substring(s.LastIndexOf("\\") + 1) + "\""; //write to console box
                make_all_buttons();
                wait_state();

                

            } else load_state();//go into wait state

        }


        ///////////////////////////////////////////////////////////////////
        /////////////////////// Touch Functions  //////////////////////////
        ///////////////////////////////////////////////////////////////////


        //Event handler for teuclick
        private void teuClick(object sender, EventArgs e) {
            Button clicked = (Button)sender;

            //Parse and figure out x&y coordinates
            int x, y;
            string location, owner, contents;
            bool isbuff = false;

            //When its on the TEU grid
            if (clicked.Name.Substring(0, 1) == "x") {
                x = Convert.ToInt32(clicked.Name.Substring(1, clicked.Name.IndexOf("_") - 1));
                y = Convert.ToInt32(clicked.Name.Substring(clicked.Name.IndexOf("_") + 1));
                location = GlobalFunctions.stringconvert(x+1) + (y+1).ToString();
                if (teugrid.getTEUAt(x, y) != null) { owner = teugrid.getTEUAt(x, y).getName(); contents = teugrid.getTEUAt(x, y).getContent(); }
                else { owner = "Unoccupied"; contents = "Unoccupied"; }
            }
            
            //When its the truck
            else if (clicked.Name.Substring(0, 3) == "tru") {
                location = "Truck";
                if (teugrid.getGoalLocation() != null) { owner = teugrid.getGoalLocation().getName(); contents = teugrid.getGoalLocation().getContent(); } 
                else { owner = "Unoccupied"; contents = "Unoccupied"; }
                
            }

              //When its on the buffer
            else {
                isbuff = true;
                string s = "";
                location = ""; owner = ""; contents = "";

                //Determine if its empty, or populate it with blocks of text
                if (teugrid.getBuffer().Count == 0) s = "Is Empty" + System.Environment.NewLine; //when empty
                for (int i = 0; i < teugrid.getBuffer().Count; i++) 
                    s+= "#"+ (i+1).ToString() + "; Owner: " + teugrid.getBuffer()[i].getName() + 
                        ", Contents: " + teugrid.getBuffer()[i].getContent() + System.Environment.NewLine;
                MessageBox.Show("Location: Buffer" + System.Environment.NewLine + s,"TEU information",MessageBoxButton.OK);
            }
            
            //Make the messagebox
            if (!isbuff) {
                MessageBox.Show("Location: " + location + System.Environment.NewLine +
                   "Owner: " + owner + System.Environment.NewLine +
                   "Contents: " + contents + System.Environment.NewLine,
                   "TEU information", MessageBoxButton.OK);
            }
        }

        //Extractbutton touch action
        private void ExtractButton_Click(object sender, RoutedEventArgs e) {

            //Check if they can click the button
            if (ExtractButton.Opacity != 1) return; 

            int x, y;

            //If user hasnt loaded a manifest, make them load it
            if (manifest.Length == 0) {
                MessageBoxResult popup = MessageBox.Show("Select Manifest File.", "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                LoadButton_Click(sender, e);
            }

            //return if manifest still does not exist
            if (manifest.Length == 0) return;

            //Prompt inputbox, get the input
            TEUextract = Interaction.InputBox("Type in the TEU you wish to Extract", "Select TEU", "");
            
            //Check validity - they inputted something into the box
            if (TEUextract.Length > 0) {
                x = GlobalFunctions.intconvert(TEUextract.Substring(0, 1));
                Int32.TryParse(TEUextract.Substring(1), out y);

                //Invalid input
                if (x == 0 || y == 0 || y > Properties.Settings.Default.MaxDock || x > Properties.Settings.Default.MaxWidth)
                    MessageBox.Show("Invalid TEU position", "Invalid", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                else if(teugrid.getTEUAt(x-1,y-1) == null)
                    MessageBox.Show("Nothing there to extract!", "Invalid", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                //Call get function and write to cnosole box
                else {

                    //set focus on consolebox
                    Consolebox.Focus();
                    FocusManager.SetFocusedElement(mygrid, Consolebox);

                    //update the grid, remove teu at truck, set console text
                    Consolebox.Text = ""; //clear the text
                    if (teugrid.clearGoalLocation()) Consolebox.Text += "Removing TEU from truck." + System.Environment.NewLine;
                    Consolebox.Text += "Extracting \"" + TEUextract + "\" (Press Enter to Continue)" + System.Environment.NewLine;
                    repopulate(false);

                    //call extract function
                    instruction_status(); //set to instruction state
                    goalx = x-1; goaly = y-1;
                    instructions = TEUGrid.extract(teugrid, goalx, goaly);
                }
            }
        }

        //Push button touch action
        private void PushButton_Click(object sender, RoutedEventArgs e) {

            //Check if they can click the button
            if (PushButton.Opacity != 1) return; 

            //Prompt Owner of the TEU
            string teuname = Interaction.InputBox("Enter TEU's owner (6+ characters in length)", "Owner's name", "");
            string teucontents = Interaction.InputBox("Enter TEU's Contents (6+ characters in length)", "Content's name", "");

            //call push function if valid input
            if (teuname.Length >= 6 && teucontents.Length >= 6) {
                Consolebox.Text = "";
                Consolebox.Text += "Pushing TEU: \"" + teuname + "\", \"" + teucontents + "\", (Press Enter to Continue)" + System.Environment.NewLine;
                goalx = -1; goaly = -1;
                instruction_status(); //set to instruction state
                instructions = TEUGrid.push(teugrid,new TEU(teuname,teucontents));
               
            } 

            //invalid input
            else 
                MessageBox.Show("TEU's owner and contents must be 6+ characters long", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        //Push buffer button touch action
        private void ClearBufferButton_Click(object sender, RoutedEventArgs e) {
            //Check if they can click the button

        }

        //Loadbutton touch action
        private void LoadButton_Click(object sender, RoutedEventArgs e) {

            //Check if they can click the button
            if (LoadButton.Opacity != 1) return; 

            //prompt the dialog box, load
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";                        //set the file extension
            dlg.Filter = "Text documents (.txt)|*.txt";     //set the file filter

            //Write to console box
            Consolebox.Text += "Loading manifest.";

            //if user selects location and saves
            if (dlg.ShowDialog() == true) {

                //reset some shit
                teugrid = new TEUGrid("");
                buttons = new Button[Properties.Settings.Default.MaxWidth, Properties.Settings.Default.MaxDock];
                manifest = "";
                Properties.Settings.Default.currentmanifest = dlg.FileName;

                //get fle contents, print to console
                StreamReader reader = new StreamReader(dlg.FileName);
                while (reader.Peek() != -1) manifest += reader.ReadLine() + System.Environment.NewLine;
                teugrid = new TEUGrid(manifest); //Shove manifest into the TEUGrid contaniner
                reader.Close();
                manifest_filename = dlg.FileName;
                Consolebox.Text += "\""+ dlg.FileName.Substring(dlg.FileName.LastIndexOf("\\")+1) + "\""; //write to console box

                make_all_buttons();

                //update consolebox text, set to wait state
                Consolebox.Text += "... Done!" + System.Environment.NewLine; //write to console box
                wait_state();
            } 
            
            else Consolebox.Text += "... Cancelled!" + System.Environment.NewLine; //Write to Console box

           
        }

        //Cast off button touch action
        private void CastOffButton_Click(object sender, RoutedEventArgs e) {

            //Check if they can click the button
            if (CastOffButton.Opacity != 1) return;
            
            //display warning
            MessageBoxResult popup = MessageBox.Show("Casting off the ship will reset the ship's contents" 
            +System.Environment.NewLine + "and push all contents of buffer to ship.", "Confirm", MessageBoxButton.OKCancel);
            if (popup.ToString() != "OK") return;
                
            //update console box
            Consolebox.Text = "Casting off ship. (Press Enter To Continue)" + System.Environment.NewLine;
            instructions = teugrid.finalizeShip(teugrid);

            //Add one last step to the instruction list
            var results = new Tuple<TEUGrid, string, int, int, int, int>(teugrid, "[FINISHED]", -1, -1, -1, -1);
            if (instructions.Count() != 0) 
                results = new Tuple<TEUGrid, string, int, int, int, int>(instructions[instructions.Count()-1].Item1, "[FINISHED]", -1, -1, -1, -1);

            casting_off = true;
            instructions.Add(results);
            instruction_status();
        }

        //Abortbutton touch action
        private void AbortButton_Click(object sender, RoutedEventArgs e) {

            //Check if they can click the button
            if (AbortButton.Opacity != 1) return; 

            //Prompt dialog box
            MessageBoxResult popup = MessageBox.Show("Abort current action?","Confirm",MessageBoxButton.OKCancel);
            
            //Abort if they pressed okay
            if (popup.ToString() == "OK") {
                wait_state();
                Consolebox.Text += "Aborted current action." + System.Environment.NewLine;
            }
        }

        //Backbutton touch action
        private void BackButton_Click(object sender, RoutedEventArgs e){

            //Check if they can click the button
            if (BackButton.Opacity != 1) return; 


            //Save current manifest


            //Switch Screens
            Switcher.Switch(new MainMenu());
        }

        ///////////////////////////////////////////////////////////////////
        /////////////////////// Helper FUnctions //////////////////////////
        ///////////////////////////////////////////////////////////////////

        //Save manifest helper function
        private void SaveManifest() {
            string pos, name, contents;
            string manifest2 = "";

            if (Properties.Settings.Default.currentmanifest == "") return;
            if (teugrid == null) return;
            if (instruction_state == -1) return;

            //update the manifest string
            for (int i = 0; i < Properties.Settings.Default.MaxWidth; i++) {
                for (int j = 0; j < Properties.Settings.Default.MaxDock; j++) {
                    pos = GlobalFunctions.stringconvert(i + 1) + (j + 1).ToString(); //get the position

                    if (teugrid.getTEUAt(i, j) != null) {
                        name = teugrid.getTEUAt(i, j).getName(); //get the company name
                        contents = ", " + teugrid.getTEUAt(i, j).getContent(); //get the contents 
                    }
                    else {
                        name = "Unoccupied";
                        contents = "";
                    }


                    if (manifest2 != "") manifest2 += System.Environment.NewLine;
                    manifest2 += pos + "\t" + name +  contents; //write to string
                }
            }

            //write to the file
            Console.WriteLine(manifest_filename);
            if (manifest2 != "" && manifest_filename != ""){
                StreamWriter sw = new StreamWriter(manifest_filename);
                using (sw) sw.Write(manifest2);
                sw.Close();
            }



        }

        //Repopulate grid
        private void repopulate(bool save) {

            //save the manifest
            if (save) SaveManifest();

            //Go through each grid
            for (int x = 0; x < teugrid.getWidth(); x++) {
                for (int y = 0; y < teugrid.getHeight(); y++) {

                    //deal with the buttons
                    if (teugrid.getTEUAt(x, y) == null) buttons[x, y].Background = Brushes.White;
                    else if (teugrid.getTEUAt(x, y).getName() == "Unoccupied" || teugrid.getTEUAt(x, y).getName() == "null")
                        buttons[x, y].Background = Brushes.White;
                    else buttons[x, y].Background = Brushes.Blue;
                }
            }

            //Go through the buffer
            Properties.Settings.Default.buffer = "";
            buffer.Background = Brushes.LightBlue;
            buffer.Content = teugrid.getBuffer().Count; 
            for (int x = 0; x < teugrid.getBuffer().Count(); x++) {
                if (teugrid.getBuffer().Count == 0) buffer.Background = Brushes.White;
                else {
                    Properties.Settings.Default.buffer += teugrid.getBuffer()[x].getName() + "," + teugrid.getBuffer()[x].getContent() + System.Environment.NewLine;
                    buffer.Content = teugrid.getBuffer().Count; 
                }
            }

            //Set the truck
            if (teugrid.getGoalLocation() == null) truck.Background = Brushes.White;
            else truck.Background = Brushes.Blue;

        }

        //Make buttons
        private void make_all_buttons() {

            //populate the screen with buttons for ship grid
            for (int i = 0; i < Properties.Settings.Default.MaxWidth; i++) {
                for (int j = 0; j < Properties.Settings.Default.MaxDock; j++) {

                    //Make the button, set height, width, and position
                    buttons[i, j] = new Button();
                    buttons[i, j].Height = 20; buttons[i, j].Width = 30;
                    buttons[i, j].Margin = new Thickness(GRIDLL_POSX + (34 * i), GRIDLL_POSY - (22.5 * j), 0, 0);
                    buttons[i, j].HorizontalAlignment = HorizontalAlignment.Left;

                    //Set parent, click event, name
                    mygrid.Children.Add(buttons[i, j]);
                    buttons[i, j].Click += new RoutedEventHandler(teuClick);
                    buttons[i, j].Name = "x" + i.ToString() + "_" + j.ToString();
                    buttons[i, j].ToolTip = GlobalFunctions.stringconvert(i + 1) + (j + 1).ToString();

                    //Determine the color
                    if (teugrid.getTEUAt(i, j) == null) buttons[i, j].Background = Brushes.White;
                    else if (teugrid.getTEUAt(i, j).getName() != "Unoccupied" && teugrid.getTEUAt(i, j).getName() != "null")
                        buttons[i, j].Background = Brushes.Blue;
                    else buttons[i, j].Background = Brushes.White;
                }

            }

            //populate the buffer
            buffer = new Button {
                Height = 20, Width = 30,
                Margin = new Thickness(BUFFER_POSX, BUFFER_POSY, 0, 0),
                Name = "buffer",
                FontSize = 12,
                HorizontalContentAlignment = HorizontalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center
            };
            buffer.Click += new RoutedEventHandler(teuClick);
            mygrid.Children.Add(buffer);
            Properties.Settings.Default.buffer = "";
            buffer.Background = Brushes.LightBlue;
            buffer.Content = teugrid.getBuffer().Count;
            for (int x = 0; x < teugrid.getBuffer().Count(); x++) {
                Properties.Settings.Default.buffer += teugrid.getBuffer()[x].getName() + "," + teugrid.getBuffer()[x].getContent() + System.Environment.NewLine;
                buffer.Content = teugrid.getBuffer().Count;
            }

            //populate truck
            truck = new Button {
                Height = 20, Width = 30,
                Margin = new Thickness(TRUCK_POSX, TRUCK_POSY, 0, 0),
                Name = "truck",
                Background = Brushes.White
            };
            truck.Click += new RoutedEventHandler(teuClick);
            mygrid.Children.Add(truck);

        }

        //Reset to load state
        private void load_state() {
            Consolebox.Text = "Load a manifest file to begin!" + System.Environment.NewLine;

            //reset some thangs
            instruction_state = -1;
            instruction_num = 0;
            r.Opacity = 0; r2.Opacity = 0;

            //Enable relevant buttons
            LoadButton.Opacity = 1;
            BackButton.Opacity = 1;
            
            //Disable relevant buttons
            AbortButton.Opacity = .5;
            CastOffButton.Opacity = .5;
            ExtractButton.Opacity = .5;
            PushButton.Opacity = .5;

        }

        //Reset to wait state
        private void wait_state() {

            //reset somethings
            if (instruction_state == 1) teugrid = new TEUGrid(teugrid);
            if (manifest.Length > 0) repopulate(true);
            instruction_state = 0; 
            instruction_num = 0;
            r.Opacity = 0; r2.Opacity = 0;

            //Enable relevant buttnos
            BackButton.Opacity = 1;
            CastOffButton.Opacity = 1;
            ExtractButton.Opacity = 1;
            PushButton.Opacity = 1;

            //Disable relevant buttons
            AbortButton.Opacity = .5;
            LoadButton.Opacity = .5;
            if (teugrid == null) {
                ExtractButton.Opacity = .5;
                PushButton.Opacity = .5;
                CastOffButton.Opacity = .5;
            }

        }

        //Reset to instruction state
        private void instruction_status() {
            instruction_state = 1;

            //set focus on consolebox
            Consolebox.Focus();
            FocusManager.SetFocusedElement(mygrid, Consolebox);

            //Enable relevant button
            AbortButton.Opacity = 1;

            //Gray out relevant buttons
            LoadButton.Opacity = .5;
            BackButton.Opacity = .5;
            CastOffButton.Opacity = .5;
            ExtractButton.Opacity = .5;
            PushButton.Opacity = .5;
        }


        ///////////////////////////////////////////////////////////////////
        /////////////////////// KEYDOWN FUnctions //////////////////////////
        ///////////////////////////////////////////////////////////////////

        //Username keydown action
        private void ConsoleBox_KeyDown(object sender, KeyEventArgs e) {

            if (e.Key == Key.Return && instruction_state == 1) { //if enter key is pressed, then

                if (instruction_num < instructions.Count()) { //when instruction state is on
                    Tuple<TEUGrid, string, int, int, int, int> instruction = instructions[instruction_num];
                   
                    if (instruction_num != 0){
                        teugrid = new TEUGrid(instructions[instruction_num - 1].Item1); 
                        repopulate(true);
                    }
                    

                    //Write instruction to conosle
                    Consolebox.Text += instruction.Item2 + "  (Press Enter to Continue)" + System.Environment.NewLine;

                    //color goal button
                    if (goalx > 0 && goaly > 0) buttons[goalx, goaly].Background = Brushes.Green;
                    

                    //Preate target rectangle (which teu to move)
                    r.Opacity = 1;
                    if (instruction.Item3 >= 0) {//regular position
                        Canvas.SetTop(r, GRIDLL_POSY - (22.5 * instruction.Item4) - 3);
                        Canvas.SetLeft(r, GRIDLL_POSX + (34 * instruction.Item3) - 3);
                    } 
                    else if (instruction.Item3 == Convert.ToInt32(TEUGrid.move_codes.BUFFER)) {//when its for the buffer
                        Canvas.SetTop(r, BUFFER_POSY - 3); //set y
                        Canvas.SetLeft(r, BUFFER_POSX -3); //set x
                    }
                    else if (instruction.Item3 == Convert.ToInt32(TEUGrid.move_codes.TRUCK)) {//when its for the truck
                        Canvas.SetTop(r, TRUCK_POSY -3); //set y
                        Canvas.SetLeft(r, TRUCK_POSX -3); //set x
                    } 
                    else r.Opacity = 0; //else its any other action

                    //Position target2 rectangle (where to move teu)
                    r2.Opacity = 1;
                    if (instruction.Item5 >= 0) {//regualr position
                        Canvas.SetTop(r2, GRIDLL_POSY - (22.5 * instruction.Item6) - 3);
                        Canvas.SetLeft(r2, GRIDLL_POSX + (34 * instruction.Item5) - 3);
                    } 
                    else if (instruction.Item5 == Convert.ToInt32(TEUGrid.move_codes.BUFFER)) {//when its for the buffer
                        Canvas.SetTop(r2, BUFFER_POSY-3); //set y
                        Canvas.SetLeft(r2, BUFFER_POSX-3); //set x
                    } 
                    else if (instruction.Item5 == Convert.ToInt32(TEUGrid.move_codes.TRUCK)) {//when its for the truck
                        Canvas.SetTop(r2, TRUCK_POSY-3); //set y
                        Canvas.SetLeft(r2, TRUCK_POSX-3); //set x
                    } 

                    else r2.Opacity = 0; //else its anyother action

                    instruction_num++;
                } 
                
                
                else {//else its the last step
                    Console.WriteLine("DEBUGGING");

                    //redraw the screen
                    teugrid = new TEUGrid(instructions[instruction_num - 1].Item1);
                    repopulate(true);

                    //WRite to the console box
                    if (!casting_off) {
                        Consolebox.Text += "Instruction Complete!!" + System.Environment.NewLine;
                        wait_state();
                    } 
                    
                    else {
                        teugrid = new TEUGrid("");
                        repopulate(false);
                        Consolebox.Text = "Ship has been cast off" + System.Environment.NewLine;
                        casting_off = false;
                        Properties.Settings.Default.currentmanifest = "";
                        load_state();
                    }

                    //reset somethings
                    
                }
                
            }
        } 

        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion

    }
}
