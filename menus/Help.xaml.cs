﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Help : UserControl, ISwitchable
    {
        //used for the index count for the pictures and descriptions arrays
        int i = 0;
        //A container for all the pictures in the help menu
        List<String> HelpPics = new List<String>(new String[] {
            "../../images/LoginHelp.bmp",
            "../../images/InvalidLogin.bmp",
            "../../images/NormalAccLoggedIn.bmp",
            "../../images/UserAccPrivilege.bmp",
            "../../images/MainMenu.bmp",
            "../../images/AdminToolsHelp.bmp",
            "../../images/UserLogHelp.bmp",
            "../../images/ClearLog.bmp",
            "../../images/SaveUserLogHelp.bmp",
            "../../images/CreateAccHelp.bmp",
            "../../images/InvalidNewAccPass.bmp",
            "../../images/AccCreatedSuccess.bmp",
            "../../images/OptionsHelp.bmp",
            "../../images/EditAccHelp.bmp",
            "../../images/EditAccBadCurrPass.bmp",
            "../../images/EditAccPassNotMatch.bmp",
            "../../images/EditAccSuccess.bmp",
            "../../images/RunExtractHelp.bmp",
            "../../images/LoadManifestHelp.bmp",
            "../../images/LoadCorrectManifest.bmp",
            "../../images/ViewContainerContents.bmp",
            "../../images/SelectTEU.bmp",
            "../../images/ExtractionProcess.bmp",
            "../../images/FinishedExtracting.bmp",
            "../../images/AbortHelp.bmp",
            "../../images/SuccessfulAbort.bmp",
            "../../images/CastOffShipHelp.bmp",
            "../../images/CastOffShipBufferProcess.bmp",
            "../../images/CastOffShipMaxHeight.bmp",
            "../../images/PushTEUowner.bmp",
            "../../images/PushTEUcontent.bmp",
            "../../images/PushTEUsuccess.bmp"
        });

        //A container for all the descriptions of the images in the help menu
        List<String> HelpDescr = new List<String>(new String[] {
            "Login Page\nThe user must have a valid username and password that should have been provided by the administrator.",
            "Invalid Login\nA message will appear if either, or both, the username or password do not match the ones stored in the system.",
            "Main Menu(Normal User)\nNormal users will have their username and their privilege status displayed in the top left corner. These users are not able to access the admin tools",
            "Invalid Access\nNormal users are prompted by a message, letting them know that they do not have permission to access the admin tools.",
            "Main Menu(Admin)\nAdministrators will have their username and admin privilege status displayed in the top left corner. They are allowed access to the admin tools as well as all the other functionality of the program.",
            "Admin Tools\nThis page is only accessible by administrators. Administrators are able to create new accounts or view the user log.",
            "UserLog\nThe user log displays a list of activities committed by the user. The log diplays the time and date that the user logged into and out of the program. It also displays whether an account has been edited or created.",
            "Clearing The Log\nWhen the Clear Log Button is clicked, a message box is displayed. It asks the users if they are sure that they want to clear the log.",
            "Saving The Log\nWhen the Save Log Button is clicked, the user will be asked to name user log and find a directory to save the file in.",
            "Create New Account\nThis page allows the administrator to create a new account. The general inputs are needed such as a user name and password. As well as confirming the password by retyping it in. When finished, click Create Account",
            "Invalid Password\nAn error message will display if either the password do not match or the password does not meet the convention.\n Password convention:\n\t-Must be 6-20 characters in length.",
            "Account Created\nA success message will be displayed if all the criteria is met. The new user will be able to log into the TEU Extractor from this point.",
            "Options Page\nThe users are allowed to manually change the value of the max TEU height while the ship is docked and the max height of the TEU that the ship can carry when sailing.\nThe default is set to 6 while docked and 5 when sailing.\nWhen finished, press the back button.",
            "Edit Account Page\nThis page allows any user to change their password. The user will need to provide their current password as well as their new password. The password must follow the convention:\n\t-Must be 6-20 characters in length.",
            "Current Password Incorrect\nAn error message will be displayed if the pasword entered for the Current Password section is incorrect.",
            "New Passwords Do Not Match\nAn error message will be displayed if the new passwords entered did not match.",
            "Edit Account Success\nA \"Success\" message will be displayed when the correct information are inputted.",
            "Run Extractor\nWhen this page is first opened, it will display an empty ship, buffer, and information box.\nThe extractor will generate a set of efficient instructions that will extract a TEU, specified by the user, from the ship and onto a truck.\nThe extractor will only run if the correct Manifest Text File is loaded to the program.",
            "Loading Manifest\nIn order to run the extractor, a Manifest must be loaded. Click on the Load Manifest Button.\nThe user will be asked to locate the manifest file from the system. The naming convention of a correct manifest is:\n\tManifest_{name_of_ship}",
            "Loading Correct Manifest\nThe information box will let the user know which file has been loaded and if the file is valid or not.",
            "View Container Contents\nThe user is able to view the items contained inside the TEU. The user can click any available blue box on the grid to view the contents.",
            "Selecting A TEU\nThe user will be prompted to enter an available TEU to extract. If the TEU is unavailable then the extractor will prompt the user that it is an invalid TEU.",
            "Extraction Process\nThe extractor will begin extracting TEUs one step at a time. When the user is ready for the next instruction, the user simply presses the Enter key. The extractor will continue extracting until either the abort button is pressed or when the green goal TEU is successfully extracted onto the truck.",
            "Finished Extracting\nA message will appear in the information box that will let the user know the extraction of the goal TEU is completed. The user is able to run another extraction with the current Manifest or load a new Manifest.",
            "Aborting The Extraction\nThe user is allowed to abort the extraction at any point in time. A message box will be displayed to confirm the user's action to abort the extraction.",
            "Successful Abort\nThe user will be notified that the extraction aborted successfully. After aborting, the user will not be able to continue the extraction that was previously running.",
            "Casting Off Ship\nWhen the user is finished extracting from the current ship, the user is able to cast it off.",
            "Casting Off: Removing Buffer Containers\nBefore the ship can leave, the buffer items will be placed back onto the ship at any available empty location.",
            "Casting Off: Adjusting to Max Height\nBefore the ship can leave, the height of the stacked containers will be adjusted if the containers are stacked higher than the allowed container height.",
            "Pushing TEU Owner\nWhen the user presses the push button, a window will pop up and ask the user for the owner of the content. The owner's name must be 6 characters long.",
            "Pushing TEU Content\nAfter inputting the owner, the user must also provide what is stored in the container. That must also be 6 characters long.",
            "Finished Pushing\nThe information box will display a message that lets the user know the push is complete. If the user clicks on the TEU from the grid then it will display the container's owner and contents that were inputted from the push."
        });

        public Help()
        {
            InitializeComponent();
        }

        //When the back button is clicked then switch back to the main menu
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainMenu());
        }

        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion

        /*A function that switches the pages of the help menu. It first clears the rich text
         * box of any strings and pictures. Then it displays the picture and description
         * at the index passed in. 
         */
        void SwitchPages(int num)
        {
            PageNum.Clear();
            InfoBox.Document.Blocks.Clear();
            ImageBrush image = new ImageBrush();
            image.ImageSource = new BitmapImage(new Uri(HelpPics[num], UriKind.Relative));
            rectangle1.Fill = image;
            InfoBox.AppendText("\n\n\n\n\n\n\n\n\n\n");
            InfoBox.AppendText(HelpDescr[num]);
            PageNum.AppendText("Page ");
            PageNum.AppendText((i + 1).ToString());
            PageNum.AppendText(" of ");
            PageNum.AppendText(HelpPics.Count.ToString());
        }


        //If "login" is selected in the menu box then it should display its corresponding help page
        private void LoginHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 0;
            SwitchPages(i);
        }

        //If "Run Extractor" is selected in the menu box then it should display its corresponding help page
        private void RunExtractorHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 17;
            SwitchPages(i);
        }

        //If "User Log" is selected in the menu box then it should display its corresponding help page
        private void UserLogHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 6;
            SwitchPages(i);
        }

        //If "Change Options" is selected in the menu box then it should display its corresponding help page
        private void ChangeOptionsHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 12;
            SwitchPages(i);
        }

        //If "Edit Account" is selected in the menu box then it should display its corresponding help page
        private void EditAccHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 13;
            SwitchPages(i);
        }

        //If "Creating New Account" is selected in the menu box then it should display its corresponding help page
        private void NewAccountHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 9;
            SwitchPages(i);
        }

        //If "Admin Tools" is selected in the menu box then it should display its corresponding help page
        private void AdminHelp_Selected(object sender, RoutedEventArgs e)
        {
            //index to the page
            i = 5;
            SwitchPages(i);
        }

        //Cycles forward through each of the pictures when the next button is clicked
        private void NextPage_Click(object sender, RoutedEventArgs e)
        {
            //index to the page is incremented
            i++;
            //if the index is greater than the amount of pictures then the index loops back to 0
            if (i >= HelpPics.Count)
                i = 0;
            SwitchPages(i);
        }

        //Cycles backwards through each of the pictures when the next button is clicked
        private void PrevPage_Click(object sender, RoutedEventArgs e)
        {
            //index to the page is decremented
            i--;
            //if the index is less than 0 then set the index to the last picture.
            if (i < 0)
                i = HelpPics.Count - 1;
            SwitchPages(i);
            
        }
    }
}
