﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for LogInWindow.xaml
    /// </summary>
    public partial class MainMenu : UserControl, ISwitchable
    {
        bool isAdmin = false;
        string username;

        public MainMenu()
        {
            InitializeComponent();

            //set the accountinfo box text
            string rawstring = Properties.Settings.Default.current_user.ToString(); //get the rawstring
            username = rawstring.Substring(0,rawstring.IndexOf(" "));        //get the username
            string level = rawstring.Substring(rawstring.LastIndexOf(" ")+1);         //get the security level
            

            //Do certain things depending on the security level
            if (level == "0") { //when user level
                level = "User Account"; 
                isAdmin = false; 
                AdminButton.Opacity = .5;
            }
            else {  //when admin level
                level = "Admin Account"; 
                isAdmin = true; 
            }

            //set account box text
            AccountInfoBox.Text = username + System.Environment.NewLine + level;
        }

        //Touch action for perference button
        private void Preference_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new Options());
        }

        //Touch actoins for help button
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new Help());
        }

        //Touch actions for admin tools button
        private void Admin_Click(object sender, RoutedEventArgs e)
        {
            if (isAdmin) Switcher.Switch(new AdminTools());

            //If not admin account, display invalid popup
            else
            {
                MessageBoxResult popup = MessageBox.Show(
                   "Requires Administrative account to access.",
                   "Invalid",
                   MessageBoxButton.OKCancel,
                   MessageBoxImage.Exclamation);
            }
        }

        //TOuch actions for extract button
        private void RunExtract_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new Extractor());
        }

        //Touch actions for logout button
        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            //Update the current user
            Properties.Settings.Default.current_user = "-1 -1 -1";

            //Update the userlog file, save settings, switch page
            Properties.Settings.Default.loguser += GlobalFunctions.gettime() + " \"" + username + "\" Logged Out." + System.Environment.NewLine;
            Properties.Settings.Default.Save();
            Switcher.Switch(new Login());
        }


        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion

    }
}
