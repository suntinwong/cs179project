﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections;
using System.Globalization;
using System.Resources;
using System.Configuration; //used to access settings.settings


    
namespace WpfApplication1
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class Login : UserControl, ISwitchable
    {
        string username;
        string password;
        List<string> usernames = new List<string>();
        List<string> passwords = new List<string>();
        List<string> adminlvl = new List<string>();
        int username_pos = -1;
        

        public Login() {
            this.InitializeComponent();
            //Properties.Settings.Default.Reset();

            //parse thorugh the Settings.settings find all account info and put into lists
            foreach (SettingsProperty  currentProperty in Properties.Settings.Default.Properties){
                string resourceKey = currentProperty.Name.ToString();
                string resourceVal = Properties.Settings.Default[currentProperty.Name].ToString();

                //add to the lists if its a user key
                if (resourceKey.Length > 4 && resourceKey.Substring(0, 4) == "user") {
                   usernames.Add(resourceVal.Substring(0, resourceVal.IndexOf(" ")));  //adding username to list
                   resourceVal = resourceVal.Substring(resourceVal.IndexOf(" ") +1);
                   passwords.Add(resourceVal.Substring(0, resourceVal.IndexOf(" "))); //adding password to list
                   resourceVal = resourceVal.Substring(resourceVal.IndexOf(" ") +1);
                   adminlvl.Add(resourceVal);   //add admin leve to list
                }
            }

            //Username.Text = "Administrator";
            //passwordBox1.Password = "password";
        }

        //Username textbox typing action
        private void Username_TextChanged(object sender, TextChangedEventArgs e) {
            int startIndex = sender.ToString().IndexOf(" ") + 1;
            username = sender.ToString().Substring(startIndex);
        }

        //Username keydown action
        private void Username_KeyDown(object sender, KeyEventArgs e){
            if (e.Key == Key.Return) //if enter key is pressed, then
                LogInButton_Click(sender, e);
        } 

        //Password box typing action
        private void passwordBox1_PasswordChanged(object sender, RoutedEventArgs e){
            password = passwordBox1.Password;
        }

        //Password box keydown action
        private void passwordBox1_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Return) //if enter key is pressed, then
                LogInButton_Click(sender, e);
        }

        //Log In button touch action
        private void LogInButton_Click(object sender, System.Windows.RoutedEventArgs e){

            //find position of the username, if it exists
            for (int i = 0; i < usernames.Count(); i++)
                if (usernames[i] == username) username_pos = i;
            
            
            //check user inputted username and password to actual ones.
            if (username_pos != -1 && usernames[username_pos] == username && passwords[username_pos] == password) {

                //set current user
                Properties.Settings.Default.current_user = username + " " + password + " " + adminlvl[username_pos];

                //Update the userlog file
                Properties.Settings.Default.loguser += GlobalFunctions.gettime() + " \"" + username + "\" Logged In." + System.Environment.NewLine;

                //Save settings file, switch screens
                Properties.Settings.Default.Save();
                Switcher.Switch(new MainMenu());
            }

            //else produce a popup box telling user its invalid
            else
            {
                MessageBoxResult popup = MessageBox.Show(
                   "Invalid Username or Password",
                   "Invalid",
                   MessageBoxButton.OK,
                   MessageBoxImage.Exclamation);
                
            }
        }


        //Quit button touch actions
        private void QuitButton_Click(object sender, RoutedEventArgs e){

            //produce popup confirmation box
            MessageBoxResult quitconfirm = MessageBox.Show(
                "Are you sure you want to quit?",
                "Quit",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Warning);
            if (quitconfirm != MessageBoxResult.Cancel)
                Application.Current.Shutdown();
            return;
        }


        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion
    }
}
