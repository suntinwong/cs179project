﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

using System.ComponentModel;
using System.IO; //used to write to file


namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for UserLog.xaml
    /// </summary>
    public partial class UserLog : UserControl, ISwitchable
    {
        public UserLog(){
            InitializeComponent();
            UserLogBox.Text = Properties.Settings.Default.loguser; //put text from loguser string to window
        }

       
        //When savelog button is pushed
        private void SaveLog_Click(object sender, RoutedEventArgs e){

            //prompt the dialog box, write to file
            Microsoft.Win32.SaveFileDialog savefiledlg = new Microsoft.Win32.SaveFileDialog();
            savefiledlg.DefaultExt = ".txt";                        //set the file extension
            savefiledlg.Filter = "Text documents (.txt)|*.txt";     //set the file filter

            //if user selects location and saves
            if (savefiledlg.ShowDialog() == true){
                using (StreamWriter sw = new StreamWriter(savefiledlg.FileName)) //write to the file
                    sw.Write(Properties.Settings.Default.loguser);               
            }
        }

        //When backbutton is pushed
        private void BackButton_Click(object sender, RoutedEventArgs e){
            Switcher.Switch(new AdminTools());
        }

        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion

        //Clears the user log
        private void ClearLogButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult Clearconfirm = MessageBox.Show(
            "Clear the User Log?",
            "Clear Log",
            MessageBoxButton.OKCancel,
            MessageBoxImage.Warning);
            if (Clearconfirm != MessageBoxResult.Cancel)
            {
                UserLogBox.Text = null;
                Properties.Settings.Default.loguser = null;
            }
            return;
        }

    }
}