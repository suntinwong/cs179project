﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration; //used to access settings.settings

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for CreateAcc.xaml
    /// </summary>
    public partial class CreateAcc : UserControl, ISwitchable
    {

        string username, password, password2,acctlvl;

        public CreateAcc()
        {
            InitializeComponent();
            acctlvl = "0";
        }

        //When create account button is clicked
        private void CreateAccButton_Click(object sender, RoutedEventArgs e)
        {

            //check if username already exists
            bool nameexists = false;
            int numusers = 0;
            foreach (SettingsProperty currentProperty in Properties.Settings.Default.Properties){
                string resourceKey = currentProperty.Name.ToString();
                string resourceVal = Properties.Settings.Default[currentProperty.Name].ToString();
                if (resourceKey.Length > 4 && resourceKey.Substring(0, 4) == "user"){
                    numusers++;
                    string temp = resourceVal.Substring(0, resourceVal.IndexOf(" "));
                    if (temp == username) { nameexists = true; }
                }
            }

            //When all fields are valid
            if (username.Length > 0 && username.Length <= 16 && password.Length >= 6 && password.Length < 20
                && password == password2 && !nameexists){

                    //Create a new user
                    var property = new SettingsProperty(Properties.Settings.Default.Properties["user0"]);//inherit from user0
                    property.Name = "user" + numusers; //set the setting name
                    Properties.Settings.Default.Properties.Add(property); //add new settings
                    Properties.Settings.Default[property.Name] = username + " " + password + " " + acctlvl; //set the value
                    Properties.Settings.Default.Save(); //save settings

                    //Updates the user log that a new user is created
                    string logstring = GlobalFunctions.gettime() + "New user \"" + username
                        + "\" created" + System.Environment.NewLine;
                    Properties.Settings.Default.loguser += logstring; 

                    //show popupbox, switch page
                    MessageBoxResult popup = MessageBox.Show("Account Created!", "Sucess!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Switcher.Switch(new AdminTools());
            }

            //else if all fields are not valid
            else{
                string invalidtext = "Invalid!";

                //invalid input; too long username
                if (username.Length > 16) invalidtext += System.Environment.NewLine + "Username is too long.";
                //username is too short
                if (username.Length < 1) invalidtext += System.Environment.NewLine + "Username is too short.";
                //username already exists
                if (nameexists) invalidtext += System.Environment.NewLine + "Username already exists.";
                //new password is too short
                if (password.Length < 6) invalidtext += System.Environment.NewLine + "Password is too short.";
                //new password is too long
                if (password.Length > 20) invalidtext += System.Environment.NewLine + "Password is too long.";
                //if new passwordws do not match
                if (password != password2) invalidtext += System.Environment.NewLine + "Passwords do not match.";
               
                //make invalid popup
                MessageBoxResult popup = MessageBox.Show(invalidtext, "Invalid", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }            
        }

        //Cancel button action
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AdminTools()); //switch back to admintool page
        }

        //Username textbox typing action
        private void Username_TextChanged(object sender, TextChangedEventArgs e)
        {
            username = sender.ToString().Substring(sender.ToString().IndexOf(" ") + 1);
        }

        //Username keydown action
        private void Username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return) //if enter key is pressed, then
                CreateAccButton_Click(sender, e);
        }

        //Password box typing action
        private void passwordBox1_PasswordChanged(object sender, RoutedEventArgs e)
        {
            password = passwordBox1.Password;
        }

        //Password box keydown action
        private void passwordBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return) //if enter key is pressed, then
                CreateAccButton_Click(sender, e);
        }

        //Confirm Password box typing action
        private void passwordBox2_PasswordChanged(object sender, RoutedEventArgs e)
        {
            password2 = passwordBox2.Password;
        }

        //Confirm Password box keydown action
        private void passwordBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return) //if enter key is pressed, then
                CreateAccButton_Click(sender, e);
        }



        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion
    }
}
