﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Options : UserControl, ISwitchable
    {
        int MaxDocknum = Properties.Settings.Default.MaxDock;
        int MaxSailnum = Properties.Settings.Default.MaxSail;
        string tempstr1 = Properties.Settings.Default.MaxDock.ToString();
        string tempstr2 = Properties.Settings.Default.MaxSail.ToString();

        //Constructor
        public Options(){
            InitializeComponent();
            //Set up inital text for the textboxes
            MaxDock.Text = Properties.Settings.Default.MaxDock.ToString();
            MaxSail.Text = Properties.Settings.Default.MaxSail.ToString();
        }

        //MaxDock Textbox text change
        private void MaxDock_TextChanged(object sender, TextChangedEventArgs e){
            tempstr1 = sender.ToString().Substring(sender.ToString().IndexOf(" ") + 1);
        }

        //MaxSail texbox text change
        private void MaxSail_TextChanged(object sender, TextChangedEventArgs e){
            tempstr2 = sender.ToString().Substring(sender.ToString().IndexOf(" ") + 1);
        }

        //Backbutton touch action
        private void BackButton_Click(object sender, RoutedEventArgs e){

            Int32.TryParse(tempstr1, out MaxDocknum);
            Int32.TryParse(tempstr2, out MaxSailnum);
            Properties.Settings.Default.MaxDock = MaxDocknum;
            Properties.Settings.Default.MaxSail = MaxSailnum;
            Properties.Settings.Default.Save();
            Switcher.Switch(new MainMenu());
        }

        //Edit button touch action
        private void EditButton_Click(object sender, RoutedEventArgs e){

            Int32.TryParse(tempstr1, out MaxDocknum);
            Int32.TryParse(tempstr2, out MaxSailnum);
            Properties.Settings.Default.MaxDock = MaxDocknum;
            Properties.Settings.Default.MaxSail = MaxSailnum;
            Properties.Settings.Default.Save();
            Switcher.Switch(new EditAcc());
        }

        //For page Switching
        #region ISwitchable Members
        public void UtilizeState(object state) { throw new NotImplementedException(); }
        #endregion
    }
}
