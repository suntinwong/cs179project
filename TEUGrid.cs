﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication1 {

    class TEUGrid {

        public enum move_codes { NIL = -1, BUFFER = -2, TRUCK = -3 };

        public enum HEURISTIC { DIJSKTRA, MANHATTAN, EUCLIDIAN };

        public HEURISTIC CURRENT_HEURISTIC = HEURISTIC.DIJSKTRA;//.DIJSKTRA;

        private int height = Properties.Settings.Default.MaxDock;
        private int width = Properties.Settings.Default.MaxWidth;
        private TEU[,] grid = new TEU[Properties.Settings.Default.MaxWidth, Properties.Settings.Default.MaxDock];
        private List<TEU> buffer = new List<TEU>();
        private TEU goalLocation = null;

        //Heuristic Values for A* search.
        private const int COST_VERT_MOVE = 42;
        private const int COST_HORI_MOVE = 6;
        private const int COST_ATTACH = 4;
        private const int COST_LETGO = 1;

        private const int BUFFER_X = 13;
        private const int BUFFER_Y = 3;
        private const int TRUCK_X = -10;
        private const int TRUCK_Y = 1;

        private int crane_X = TRUCK_X;
        private int crane_Y = TRUCK_Y;

        //Special Instruction Message definitions
        static private string instructToGoalFrom(int x, int y) { return string.Format("Move TEU at {0}{1} to Loading Area.", coorToString(x), Convert.ToString(y + 1));}
        static private string instructToBuffFrom(int x, int y) { return string.Format("Move TEU at {0}{1} to Buffer Area.", coorToString(x), Convert.ToString(y + 1)); }
        static private string instructFromBuffTo(TEU teu, int x, int y) { return string.Format("Move TEU {0} in Buffer to {1}{2}.", teu.getName(), coorToString(x), Convert.ToString(y + 1)); }
        static private string instructFromTo(int x1, int y1, int x2, int y2) { return string.Format("Move TEU at {0}{1} to {2}{3}.", coorToString(x1), Convert.ToString(y1 + 1), coorToString(x2), Convert.ToString(y2 + 1)); }
        static private string instructFromGoalTo(int x, int y) { return string.Format("Move TEU at Loading Area to {0}{1}.", coorToString(x), Convert.ToString(y + 1)); }

        //Instructions with distance printing
        static private string instructToGoalFrom(int x, int y, int dist, int totalDist) { return string.Format("Move TEU at {0}{1} to Loading Area. [ETA: {2} seconds]\nTotal time elapsed: {3} seconds", coorToString(x), Convert.ToString(y + 1), dist, totalDist); }
        static private string instructToBuffFrom(int x, int y, int dist) { return string.Format("Move TEU at {0}{1} to Buffer Area. [ETA: {2} seconds].", coorToString(x), Convert.ToString(y + 1), dist); }
        static private string instructFromBuffTo(TEU teu, int x, int y, int dist) { return string.Format("Move TEU {0} in Buffer to {1}{2}. [ETA: {2} seconds]", teu.getName(), coorToString(x), Convert.ToString(y + 1), dist); }
        static private string instructFromTo(int x1, int y1, int x2, int y2, int dist) { return string.Format("Move TEU at {0}{1} to {2}{3}. [ETA: {4} seconds].", coorToString(x1), Convert.ToString(y1 + 1), coorToString(x2), Convert.ToString(y2 + 1), dist); }

        //Converts int Coordinates into corresponding letters
        static private string coorToString(int x) {
            switch (x) {
                case 0: return "A";
                case 1: return "B";
                case 2: return "C";
                case 3: return "D";
                case 4: return "E";
                case 5: return "F";
                case 6: return "G";
                case 7: return "H";
                case 8: return "I";
                case 9: return "J";
                default:
                    System.Diagnostics.Debug.Assert(true, "ERROR: INVALID X COORDINATE");
                    return "?";
            }
        }


        ////////////////////////////////////////////////////////////
        //CONSTRUCTORS
        ////////////////////////////////////////////////////////////

        /// <summary>
        /// Creates a deep copy of original object.
        /// </summary>
        /// <param name="original">The object to create a deep copy of.</param>
        public TEUGrid(TEUGrid original) {
            height = original.getHeight();
            width = original.getWidth();
            goalLocation = original.getGoalLocation();

            crane_X = original.getCraneX();
            crane_Y = original.getCraneY();

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    grid[x, y] = original.getTEUAt(x, y);
                }
            }

            foreach (TEU teu in original.getBuffer()) {
                buffer.Add(teu);
            }
        }


        /// <summary>
        /// Gets the manifest, parses it and populate in a TEUGrid object.
        /// </summary>
        /// <param name="manifest">String containing the manifest to be parsed into a TEUGrid</param>
        public TEUGrid(string manifest) {

            //Initialize everything to null
            grid = new TEU[Properties.Settings.Default.MaxWidth, Properties.Settings.Default.MaxDock];
            for (int i = 0; i < width; i++) for (int j = 0; j < height; j++) grid[i, j] = null;// new TEU("null", "null");
            buffer.Clear();

            //Parse through the entire string
            string tstring = manifest;
            string name, contents, rawstring;
            int x, y;
            while (tstring.Length > 1) {

                //Determine x,y coordinates
                x = GlobalFunctions.intconvert(tstring.Substring(0, 1));
                y = Convert.ToInt32(tstring.Substring(1, 1));

                //Determine name and contents - set to Unoccupied if its unoccupied
                rawstring = tstring.Substring(3, tstring.IndexOf("\n") - 4);
                if (rawstring != "Unoccupied") {
                    name = rawstring.Substring(0, rawstring.IndexOf(","));
                    contents = rawstring.Substring(rawstring.IndexOf(",") + 2);
                    grid[x - 1, y - 1] = new TEU(name, contents);
                }

                tstring = tstring.Substring(tstring.IndexOf("\n") + 1); //Update temp string value, add to grid
            }

            //update the buffer
            tstring = Properties.Settings.Default.buffer;
            Console.WriteLine(Properties.Settings.Default.buffer);
         
            while (tstring.Length > 1) {
                rawstring = tstring.Substring(0, tstring.IndexOf("\n") - 1);
                name = rawstring.Substring(0, rawstring.IndexOf(","));
                contents = rawstring.Substring(rawstring.IndexOf(",") + 1);
                tstring = tstring.Substring(tstring.IndexOf("\n") + 1);

                buffer.Add(new TEU(name, contents));
            }
        }


        ////////////////////////////////////////////////////////////
        //GETTERS AND SETTERS
        ////////////////////////////////////////////////////////////

        public int getHeight() { return height; }

        public int getWidth() { return width; }

        public List<TEU> getBuffer() { return buffer; }

        public TEU getGoalLocation() { return goalLocation; }

        private void setGoalLocation(TEU teu) { goalLocation = teu; }

        private void setTEUAt(TEU teu, int x, int y) { grid[x, y] = teu; }

        public TEU getTEUAt(int x, int y) {
            if (x < 0 || x >= width || y < 0 || y >= height || grid[x, y] == null) {
                return null;
            }
            return grid[x, y];
        }

        private int getCraneX() { return crane_X; }

        private int getCraneY() { return crane_Y; }

        private void setCraneCoordinates(int x, int y) {
            crane_X = x;
            crane_Y = y;
        }


        /// <summary>
        /// Dummy Extraction Function. Used only for testing and debugging.
        /// </summary>
        /// <param name="x">x value of TEU to extract</param>
        /// <param name="y">y value of TEU to extract</param>
        /// <returns></returns>
        public List<Tuple<TEUGrid, string, int, int, int, int>> extractDUMMY(int x, int y) {

            var list = new List<Tuple<TEUGrid, string,int,int,int,int>> ();
            TEUGrid temp;
            
            //Move A2 to B5
            grid[4, 1] = grid[1, 0];                            //set a5 to a2
            grid[1, 0] = new TEU("Unoccupied", "Unoccupied");   //set a2 to unoccupied
            temp = new TEUGrid(this);                      //make deep copy
            list.Add(new Tuple<TEUGrid, string, int, int, int, int>(temp, "Move A2 to B5", 1, 0, 4, 1));

            //Move A4 to b9
            grid[8, 1] = grid[3, 0];                            //set b9 to a4
            grid[3, 0] = new TEU("Unoccupied", "Unoccupied");   //set a4 to unoccupied
            temp = new TEUGrid(this);                      //make deep copy
            list.Add(new Tuple<TEUGrid, string, int, int, int, int>(temp, "Move A4 to B9", 3, 0, 8, 1));

            //Move a5 to buffer
            buffer.Add( new TEU(grid[4,0].getName(),grid[4,0].getContent()));   //add a5 to buffer
            grid[4, 0] = new TEU("Unoccupied", "Unoccupied");                   //set a5 to unoccupied
            temp = new TEUGrid(this);                                      //make deep copy
            list.Add(new Tuple<TEUGrid, string, int, int, int, int>(temp, "Move A5 to BUFFER", 4, 0, -2, -2));

            //Move a1 to truck
            //set truck to a1????
            grid[0, 0] = new TEU("Unoccupied", "Unoccupied");                   //set a1 to unoccupied
            temp = new TEUGrid(this);                                      //make deep copy
            list.Add(new Tuple<TEUGrid, string, int, int, int, int>(temp, "Move A1 to Truck", 0, 0, -3, -3)); 


            return list;
        }


        ////////////////////////////////////////////////////////////
        //Grid Editing Functions
        ////////////////////////////////////////////////////////////

        private void moveTEU(int fromX, int fromY, int toX, int toY) {
            //Console.WriteLine("from: ("+fromX+","+fromY+") to: ("+toX+","+toY+")");
            if (grid[toX, toY] != null) return;
            grid[toX, toY] = grid[fromX, fromY];
            removeTEUAt(fromX, fromY);
        }

        private void moveTEUfromBuffer(int toX, int toY) {
            if (grid[toX, toY] != null || buffer.Count <= 0) return;
            grid[toX, toY] = buffer.ElementAt(0);
            buffer.RemoveAt(0);
        }

        /// <summary>
        /// Removes the TEU at coordinates (x,y)
        /// </summary>
        /// <returns>returns true if successfull</returns>
        public bool removeTEUAt(int x, int y) {
            if (x < 0 || x >= width || y < 0 || y >= height || grid[x, y] == null) {
                return false;
            }

            grid[x, y] = null;
            return true;
        }

        public bool clearGoalLocation() {
            if (goalLocation == null) { return false; }
            goalLocation = null;
            return true;
        }

        ////////////////////////////////////////////////////////////
        //METHODS
        ////////////////////////////////////////////////////////////

        //Prints the Name&Contents of the TEUGrid 
        public void print() {
            for (int i = 0; i < width; i++) for (int j = 0; j < height; j++) {
                    if(grid[i,j] == null)
                        Console.WriteLine("[" + i + "][" + j + "]:" + "ACTUAL NULL" + "," + "ACTUAL NULL");
                    else  
                        Console.WriteLine("[" + i + "][" + j + "]:" + grid[i, j].getName() + "," + grid[i, j].getContent());
                }
        }

        
        //A* extractor. Moves everything to the buffer to get to the main stuff.
        static public List<Tuple<TEUGrid, string, int, int, int, int>> extract(TEUGrid grid, int x, int y) {

            int nodesAdded = 0;
            int nodesExplored = 0;

            List<Tuple<TEUGrid, string, int, int, int, int>> results = new List<Tuple<TEUGrid, string, int, int, int, int>>();

            //Create list of sorted tuples to act as a priority heap.
            //Object type represents: node, distance, pathThusFar
            List< Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string,int,int,int,int>> > > searchTree = new List< Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string,int,int,int,int>> > >();
            searchTree.Sort((a, b) => a.Item3.CompareTo(b.Item3));

            Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string,int,int,int,int>> > startNode = new Tuple<TEUGrid,int,int,List<Tuple<TEUGrid,string,int,int,int,int>>>(new TEUGrid(grid),0,0, new List<Tuple<TEUGrid,string,int, int, int, int>>() );
            searchTree.Add(startNode);
            nodesAdded++;

            //Limited Breadth First Search. Should only go a depth equal to the height (at most.
            for (int h = grid.getHeight() - 1; h >= 0 && searchTree.Count > 0; h = grid.getHeight() - 1) {
                searchTree.Sort((a, b) => a.Item3.CompareTo(b.Item3));
                
                Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string,int,int,int,int>> > currNode = searchTree.First<Tuple<TEUGrid,int,int, List<Tuple<TEUGrid, string,int,int,int,int>> >>();
                searchTree.RemoveAt(0);
                nodesExplored++;

                //Make deep copies
                TEUGrid currTEUGrid = new TEUGrid(currNode.Item1);
                int currDist = currNode.Item2;
                int currCost = currNode.Item3;
                List<Tuple<TEUGrid, string,int, int, int, int>> currInstructions = new List<Tuple<TEUGrid,string,int,int,int,int>>(currNode.Item4);

                //Console.WriteLine("Opening New Node with Cost: " + currCost);

                h = currTEUGrid.getMaxColumnHeight(x);

                //This means the goal node has been explored
                if (y > h) {
                    results = currInstructions;

                    Console.WriteLine(
                        "EXTRATION REPORT\n" +
                        "Space (# of nodes Added): " + nodesAdded + "\n" +
                        "Time (# of nodes Explored): " + nodesExplored + "\n" +
                        "Time (in seconds used): " + currDist);

                    return results;
                }

                //Add the goal node.
                if(h == y){
                    int oldDist = currDist;
                    currDist = currDist + COST_ATTACH + COST_LETGO + getMoveCost(currTEUGrid, x, y, TRUCK_X, TRUCK_Y);
                    
                    //Adds distance for time to move crane
                    currDist += getMoveCost(currTEUGrid, currTEUGrid.getCraneX(), currTEUGrid.getCraneY(), x, y);
                    currTEUGrid.setCraneCoordinates(TRUCK_X, TRUCK_Y); //Set coordinates of crane to truck's position

                    currTEUGrid.setGoalLocation(currTEUGrid.getTEUAt(x, y));
                    currTEUGrid.removeTEUAt(x, y);
                    currInstructions.Add(new Tuple<TEUGrid, string,int,int,int,int>(currTEUGrid, instructToGoalFrom(x,y,currDist-oldDist,currDist),x,y,(int)move_codes.TRUCK,(int)move_codes.TRUCK) );
                    searchTree.Add(new Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string, int, int, int, int>>>(currTEUGrid, currDist, currCost, currInstructions));
                    nodesAdded++;

                }

                
                //Add to buffer operation to search tree. -------------------------------------------------------------
                if (currTEUGrid.getTEUAt(x, h) != null && h > y) {
                    //Make new deep copies, old ones were edited
                    currTEUGrid = new TEUGrid(currNode.Item1);
                    currDist = currNode.Item2;
                    currInstructions = new List<Tuple<TEUGrid, string,int,int,int,int>>(currNode.Item4);

                    int oldDist = currDist;
                    currDist = currDist + COST_ATTACH + COST_LETGO + getMoveCost(currTEUGrid, x, h, BUFFER_X, BUFFER_Y);
                    
                    //Adds distance for time to move crane
                    currDist += getMoveCost(currTEUGrid, currTEUGrid.getCraneX(), currTEUGrid.getCraneY(), x, h);
                    currTEUGrid.setCraneCoordinates(BUFFER_X, BUFFER_Y); //Set coordinates of crane to buffers's position

                    currCost = currDist + grid.getHeurstic(x, h, BUFFER_X, BUFFER_Y);
                    
                    //Console.WriteLine("COST from (" + x + "," + h + ") to (" + BUFFER_X + "," + BUFFER_Y + ") is" + currCost + ". Dist is " + currDist);

                    currTEUGrid.getBuffer().Add(currTEUGrid.getTEUAt(x, h));
                    currTEUGrid.removeTEUAt(x, h);
                    currInstructions.Add(new Tuple<TEUGrid, string, int, int, int, int>(currTEUGrid, instructToBuffFrom(x, h, currDist - oldDist), x, h, (int)move_codes.BUFFER, (int)move_codes.BUFFER));
                    searchTree.Add(new Tuple<TEUGrid,int,int,List<Tuple<TEUGrid,string,int,int,int,int>>>(currTEUGrid, currDist, currCost, currInstructions));
                    nodesAdded++;


                    //Add 'move to other columns' operation to search tree.-------------------------------------------------------

                    //Make new deep copies, old ones were edited
                    currTEUGrid = new TEUGrid(currNode.Item1);
                    int maxWidth = grid.getWidth();
                    int maxHeight = grid.getHeight();
                    //First find height of teu stack to left. 
                    int y2 = currTEUGrid.getHeight() - 1;
                    int x2 = 0;

                    //Cannot move left if x is already 0.
                    for (x2 = 0; x2 < maxWidth; x2++ ) {

                        //Do not put in same column, pulling from
                        if (x2 == x) { continue; }

                        y2 = maxHeight - 1;
                        //Make new deep copies, old ones were edited
                        currTEUGrid = new TEUGrid(currNode.Item1);
                        currDist = currNode.Item2;
                        currInstructions = new List<Tuple<TEUGrid, string, int, int, int, int>>(currNode.Item4);

                        //Find the height of the column.
                        for (; (y2 >= 0) && (currTEUGrid.getTEUAt(x2, y2) == null); y2--) ;

                        //Add one because TEU goes on top of the non-empty space.
                        y2++;

                        //Do not continue if the stack is at its maximum height, instead look to the left one more column.
                        if (y2 <= currTEUGrid.getHeight() - 1) {
                            oldDist = currDist;
                            currDist = currDist + COST_ATTACH + COST_LETGO + getMoveCost(currTEUGrid, x, h, x2, y2);

                            //Adds distance for time to move crane
                            currDist += getMoveCost(currTEUGrid, currTEUGrid.getCraneX(), currTEUGrid.getCraneY(), x, h);
                            currTEUGrid.setCraneCoordinates(x2, y2); //Set coordinates of crane to the position of the moved TEU
                            
                            currCost = currDist + grid.getHeurstic(x, h, x2, y2);

                            //Console.WriteLine("COST from (" + x + "," + h + ") to (" + leftX + "," + leftY + ") is" + currCost + ". Dist is " + currDist);

                            currTEUGrid.moveTEU(x, h, x2, y2);
                            currInstructions.Add(new Tuple<TEUGrid, string, int, int, int, int>(currTEUGrid, instructFromTo(x, h, x2, y2,currDist-oldDist), x, h, x2, y2));

                            searchTree.Add(new Tuple<TEUGrid, int, int, List<Tuple<TEUGrid, string, int, int, int, int>>>(currTEUGrid, currDist, currCost, currInstructions));
                            nodesAdded++;
                        }
                    }
                }

            }

            System.Diagnostics.Debug.Assert(true, "TEU NOT FOUND. NO INSTRUCTIONS TO PASS");
            return results;
        }


        public List<Tuple<TEUGrid, string, int, int, int, int>> finalizeShip(TEUGrid grid) {
            //Console.WriteLine("FINALIZING SHIP:");
            
            List<Tuple<TEUGrid, string, int, int, int, int>> results = new List<Tuple<TEUGrid, string, int, int, int, int>>();
            TEUGrid temp = new TEUGrid(grid);
            //Console.WriteLine("\tBUFFER RELOADING");

            //Move everything from the buffer to the ship
            for (int i = temp.getWidth() - 1; (temp.getBuffer().Count > 0) && (i >= 0); i--) {
                for (int j = 0; (temp.getBuffer().Count > 0) && (j < temp.getHeight() - 1); j++){
                    if (temp.getTEUAt(i, j) == null) {
                        temp.moveTEUfromBuffer(i, j);
                        temp.setCraneCoordinates(i, j);

                        TEUGrid newGrid = new TEUGrid(temp); //Make new deep copies
                        results.Add(new Tuple<TEUGrid, string, int, int, int, int>(newGrid, instructFromBuffTo(temp.getTEUAt(i, j), i, j), (int)move_codes.BUFFER, (int)move_codes.BUFFER, i, j));
                    }
                }
                
            }


            //Move any of the TEUs to the appropriate stack height
            //Console.WriteLine("\n\t HEIGHT RESIZING:");

            int maxHeight = Properties.Settings.Default.MaxSail;
            //Console.WriteLine("\t MAXSAIL HEIGHT = " + maxHeight);

            for (int i = 0; i < temp.getWidth(); i++) {
                for (int j = temp.getHeight() - 1; j >= maxHeight; j--) {
                    //Console.WriteLine("\t Checking if too heigh "+ i + " " +j);
                    if (temp.getTEUAt(i, j) != null) {
                        //Console.WriteLine("\tFound TEU to relocate at " +i + " " +j);
                        for (int x = 0; x < temp.getWidth(); x++) {
                            bool isDone = false;
                            for (int y = 0; y < maxHeight && !isDone; y++) {
                                //Console.WriteLine("\t\tChecking for empty space at " + x + " " + y);
                                if (temp.getTEUAt(x, y) == null) {
                                    //Console.WriteLine("\t\tFound empty space for TEU at "+ x + " " +y);
                                    isDone = true;
                                    temp.moveTEU(i, j, x, y);
                                    temp.setCraneCoordinates(i, j);
                                    //Make new deep copies, old ones were edited
                                    TEUGrid newGrid = new TEUGrid(temp);
                                    results.Add(new Tuple<TEUGrid, string, int, int, int, int>(newGrid, instructFromTo(i, j, x, y),i,j,x,y) );
                                    break;
                                }//end if
                            }
                            if (isDone) { break; }
                        }
                    }//end if

                }

            }

            return results;
        }

        

        /// <summary>
        /// Adds the TEU to the position closest to the Loading Truck (goalLoacation)
        /// </summary>
        /// <param name="grid">The TEUGrid to add to</param>
        /// <param name="teu">The TEU to add</param>
        static public List<Tuple<TEUGrid, string, int, int, int, int>> push(TEUGrid grid, TEU teu) {
            List<Tuple<TEUGrid, string, int, int, int, int>> results = new List<Tuple<TEUGrid, string, int, int, int, int>>();
            TEUGrid temp = new TEUGrid(grid);
            for (int i = 0; i < temp.getWidth(); i++) {
                for (int j = 0; j < temp.getHeight(); j++) {
                    if (temp.getTEUAt(i, j) == null) {
                        temp.setTEUAt(teu, i, j);
                        temp.setCraneCoordinates(i, j);
                        results.Add(new Tuple<TEUGrid, string, int, int, int, int>(temp, instructFromGoalTo(i, j), 
                            Convert.ToInt32(move_codes.NIL), Convert.ToInt32(move_codes.NIL), i, j));
                        return results;
                    }
                }
            }
            return results;
        }

        private int getMaxColumnHeight(int columnX) {
            int h = getHeight() - 1;
            while (h >= 0) {
                if (getTEUAt(columnX, h) != null) { break; }
                if (h < 0) { break; }
                h--;
            }

            return h;
        }

        //Finds cost with primative path finding.
        private static int getMoveCost(TEUGrid grid, int fromX, int fromY, int toX, int toY) {
            int cost = 0;

            int leftX;
            int leftY;
            int rightX;
            int rightY;

            //Go left
            if ((fromX - toX) > 0) {
                leftX = toX;
                leftY = toY;
                rightX = fromX;
                rightY = fromY;
            }

            //Go right
            else if ((fromX - toX) < 0) {
                leftX = fromX;
                leftY = fromY;
                rightX = toX;
                rightY = toY;
            }

            //If X locations are the same, just return 0.
            else {
                return 0;
            }

            //Console.WriteLine("lx = " + leftX + "\nly = " + leftY + "\nrx = " + rightX + "\nrl = " + rightY);

            int startX = leftX;
            if (leftX == BUFFER_X && leftY == BUFFER_Y) { startX = 0; }

            int endX = rightX;
            if (rightX == TRUCK_X && rightY == TRUCK_Y) { endX = grid.getWidth(); }
            
            int minClearHeight = -1;

            for (int i = startX; i <= endX; i++) {
                int tempHeight = grid.getMaxColumnHeight(i);
                if (i == fromX) { tempHeight--; }
                minClearHeight = Math.Max(minClearHeight, tempHeight); 
                //Console.WriteLine("temp : " + tempHeight + " NEWMIN: " + minClearHeight);
            };

            //Add two because the TEU cannot be "dragging" on the bottom edge.
            minClearHeight+=2;

            //Special Cases where the clearance height might be equivalent to positions off the ship
            if ((fromX == TRUCK_X && fromY == TRUCK_Y) || (toX == TRUCK_X && toY == TRUCK_Y)) { minClearHeight = Math.Max(TRUCK_Y + 1, minClearHeight); }
            if ((fromX == BUFFER_X && fromY == BUFFER_Y) || (toX == BUFFER_X && toY == BUFFER_Y)) { minClearHeight = Math.Max(BUFFER_Y + 1, minClearHeight); }

            //Calculate the cost of vertical and horizontal moves.
            cost += (COST_VERT_MOVE * ((minClearHeight - rightY) + (minClearHeight - leftY)));
            cost += (COST_HORI_MOVE * (rightX - leftX)); 

            //Console.WriteLine("(" + fromX + "," + fromY + ") to" + "(" + toX + "," + toY + ")" + " Min Height = " + minClearHeight + "Cost = " + cost);

            return cost;
        }


        private int getHeurstic(int fromX, int fromY, int toX, int toY){
            switch (CURRENT_HEURISTIC) {
                case HEURISTIC.DIJSKTRA:
                    return 0;
                case HEURISTIC.MANHATTAN:
                    return getManhattanDistance(fromX, fromY, toX, toY);
                case HEURISTIC.EUCLIDIAN:
                    return getEuclidianDistance(fromX, fromY, toX, toY);
                default:
                    System.Diagnostics.Debug.Assert(true, "INVALID HEURISTIC VARIABLE");
                    return 0;
            }
        }

        //Manhattan Distance Heuritic
        private int getManhattanDistance(int fromX, int fromY, int toX, int toY) {
            return ((int)Math.Abs(fromX - toX) + (int)Math.Abs(fromY - toY));
        }

        //Euclidean Distance Heuritic
        private int getEuclidianDistance(int fromX, int fromY, int toX, int toY) {
            int x2 = (int)Math.Pow((int)Math.Abs(fromX - toX),2);
            int y2 = (int)Math.Pow((int)Math.Abs(fromY - toY),2);
            int result = (int)Math.Sqrt(x2 + y2);
            return result;
        }

    }




    class TEU {
        private string name;
        private string contents;

        public TEU(string pname, string pcontents) {
            name = pname;
            contents = pcontents;
        }

        public string getName(){
            return name;
        }

        public string getContent() {
            return contents;
        }

    }





    class Node {
        private int cost;
        private int distance;
        private Instruction instruction;
        private TEUGrid grid;


        public Node(TEUGrid pGrid, int pDistance, int pCost, Instruction pInstruction) {
            grid = pGrid;
            distance = pDistance;
            cost = pCost;
            instruction = pInstruction;
        }
    }





    class Instruction {
        private TEUGrid grid;
        private string instruction;
        private int fromX;
        private int fromY;
        private int toX;
        private int toY;

        public Instruction(TEUGrid pGrid, string pInctruction, int pFromX, int pFromY, int pToX, int pToY) {
            grid = pGrid;
            instruction = pInctruction;
            fromX = pFromX;
            fromY = pFromY;
            toX = pToX;
            toY = pToY;
        }

        public int getFromX(){
            return fromX;
        }

        public int getFromY(){
            return fromY;
        }

        public int getToX(){
            return toX;
        }

        public int getToY(){
            return toY;
        }

        public string getInstruction(){
            return instruction;
        }

        public TEUGrid getTEUGrid(){
            return grid;
        }
    }
}
