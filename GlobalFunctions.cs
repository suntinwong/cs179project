﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication1
{
    public static class GlobalFunctions
    {

        //Gets the current date and time, returns a string
        public static string gettime(){

            //Calculates the time the user logs into the iTEU_App
            String timeOfDay = "";
            String currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            String hourNow = DateTime.Now.ToString("HH");
            int hour = Convert.ToInt32(hourNow);

            //If the hour is past 12 but not 24 then the time of day is "pm"
            if (hour > 11 && hour != 24)
                timeOfDay = "pm";
            //Otherwise, it is "am"
            else
                timeOfDay = "am";
            String currentHour;
            if (hour > 12) currentHour = Convert.ToString(hour - 12); //Checks if the hour is larger than 12
            else if (hour == 0) currentHour = Convert.ToString(12); //If the hour is 0 then it is 12am
            else currentHour = hour.ToString(); //Else return the hour
            
            String currentMinute = DateTime.Now.ToString("mm");
            String currentTime = currentHour + ":" + currentMinute + timeOfDay;

            return currentDate + ", " + currentTime + ":";
        }

        //Converts string to a number, used for determining placement of X coordinate
        //Only returns up to 10, otherwise returns 0
        public static int intconvert(string s) {
            if (s == "A" || s == "a") return 1;
            else if (s == "B" || s == "b") return 2;
            else if (s == "C" || s == "c") return 3;
            else if (s == "D" || s == "d") return 4;
            else if (s == "E" || s == "e") return 5;
            else if (s == "F" || s == "f") return 6;
            else if (s == "G" || s == "g") return 7;
            else if (s == "H" || s == "h") return 8;
            else if (s == "I" || s == "i") return 9;
            else if (s == "J" || s == "j") return 10; else return 0;
        }

        //Converts intger to upper case letter
        public static string stringconvert(int s) {
            if (s == 1) return "A";
            else if (s == 2) return "B";
            else if (s == 3) return "C";
            else if (s == 4) return "D";
            else if (s == 5) return "E";
            else if (s == 6) return "F";
            else if (s == 7) return "G";
            else if (s == 8) return "H";
            else if (s == 9) return "I";
            else if (s == 10) return "J";
            else return "0";

        }


    }

     
}
